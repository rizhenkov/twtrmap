[![pipeline status](https://gitlab.com/rizhenkov/twtrmap/badges/master/pipeline.svg)](https://gitlab.com/rizhenkov/twtrmap/commits/master)

# Real-time twitter map

It shows locations, added to tweets in real-time.  See [online demo](https://twtrmap.herokuapp.com/)

![](https://gitlab.com/rizhenkov/twtrmap/uploads/57436aaf053967004f01dca974f06d2e/demo.gif)

Created just for fun.


## Requirements

- NodeJS with npm
- Hosting with sockets support

## Installation

You need to create [new Twitter APP](https://apps.twitter.com/app/new) and set your credentials data to index.js

`git clone https://gitlab.com/rizhenkov/twtrmap.git`

`cd twtrmap`

`npm install`

`npm start`

on Heroku you need also run

`heroku features:enable http-session-affinity`

## Based on open libraries and free services

- [twit](https://github.com/ttezel/twit) Twitter API Client for node (REST & Streaming API)
- [Socket.io](https://socket.io/) Real-time engine
- [Express](https://expressjs.com/) Minimalist web framework
- [Yandex.Maps API](https://tech.yandex.ru/maps/)
- [Twitter API](https://developer.twitter.com/)