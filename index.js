'use strict';

const express = require('express');
const socketIO = require('socket.io');
const path = require('path');
const Twit = require('twit');

const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, 'index.html');

const server = express()
    .use('/io-lib.js', (req, res) => res.sendFile(path.join(__dirname, '/node_modules/socket.io-client/dist/socket.io.slim.js')) )
    .use('/puff.svg', (req, res) => res.sendFile(path.join(__dirname, 'puff.svg')) )
    .use('/style.css', (req, res) => res.sendFile(path.join(__dirname, 'style.css')) )
    .use('/favicon-16x16.png', (req, res) => res.sendFile(path.join(__dirname, 'favicon-16x16.png')) )
    .use('/favicon-32x32.png', (req, res) => res.sendFile(path.join(__dirname, 'favicon-32x32.png')) )
    .use('/favicon.ico', (req, res) => res.sendFile(path.join(__dirname, 'favicon.ico')) )
    .use((req, res) => res.sendFile(INDEX) )
    .listen(PORT, () => console.log(`Listening on ${ PORT }`));

const io = socketIO(server);


const twitterCredentials = {
    consumer_key: '',
    consumer_secret: '',
    access_token: '',
    access_token_secret: '',
    timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
};

if(process.env.TWTR_CONS_KEY){
    twitterCredentials.consumer_key = process.env.TWTR_CONS_KEY;
}
if(process.env.TWTR_CONS_SEC){
    twitterCredentials.consumer_secret = process.env.TWTR_CONS_SEC;
}
if(process.env.TWTR_ACCS_TKN){
    twitterCredentials.access_token = process.env.TWTR_ACCS_TKN;
}
if(process.env.TWTR_ACCS_SEC){
    twitterCredentials.access_token_secret = process.env.TWTR_ACCS_SEC;
}


const T = new Twit(twitterCredentials);


io.on('connection', function (socket) {

    let stream = null;

    socket.on('stream start', function (data) {

        if (stream) {
            stream.stop();
            stream = null;
        }

        stream = T.stream('statuses/filter', {
            // track: trackWords,
            // language: 'ru',
            //filter_level: 'medium',
            locations: clearInputs(data.bounds)
        });

        stream.on('tweet', function (tweet) {

            if (tweet.place) {

                if (!tweet.coordinates) {
                    tweet.coordinates = {
                        type: "Point",
                        coordinates: [
                            randomInRange(tweet.place.bounding_box.coordinates[0][0][1],
                                tweet.place.bounding_box.coordinates[0][2][1]),
                            randomInRange(tweet.place.bounding_box.coordinates[0][0][0],
                                tweet.place.bounding_box.coordinates[0][2][0])
                        ]
                    };
                }

                io.emit('new tweet', {
                    tweet: tweet
                });
            }

        });

        stream.on('limit', function (limitMessage) {
            socket.emit('stream status', {
                status: 'limit',
                message: 'Limit reached, dropping frames'
            });
        });

        stream.on('disconnect', function (disconnectMessage) {
            socket.emit('stream status', {
                status: 'disconnect',
                message: 'Disconnected from stream!'
            });
        });

        stream.on('connect', function (request) {
            socket.emit('stream status', {
                status: 'connect',
                message: 'Connecting to stream...'
            });
        });

        stream.on('connected', function (res) {
            socket.emit('stream status', {
                status: 'connected',
                message: 'Connected to stream, receiving data'
            });
        });

        stream.on('reconnect', function (request, response, connectInterval) {
            socket.emit('stream status', {
                status: 'reconnect',
                message: 'Trying to reconnect...'
            });
        });

    });

    socket.on('stream stop', function () {
        if (stream) {
            stream.stop();
        }
    });

    socket.on('disconnect', function () {
        if (stream) {
            stream.stop();
        }
    });
});


/**
 * We need this function to draw square locations
 * @param min
 * @param max
 * @returns {*}
 */
function randomInRange(min, max) {
    return Math.random() < 0.5 ? ((1 - Math.random()) * (max - min) + min) : (Math.random() * (max - min) + min);
}


function clearInputs(bounds) {
    let clean = [];

    bounds.forEach(function (coods) {
        clean.push(parseFloat(coods))
    });

    return clean;
}